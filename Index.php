<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="estilos.css"/>
    <title>Hello, world!</title>
  </head>
  <body style="background-color: #f4f1f1;">
      <div id="contenido" class="container  col-lg-6 col-lg-offset-3 shadow-sm p-3 mb-5 bg-white">
          <div id="contenido2" class="panel panel-default">
            <div class="panel-feading">
                <div class="row">
                     <div id="fotoform" class="col-3">
                    </div>
                    <div class="col-9">
                        <center><p class="h3">Registro de usuarios</p></center>
                        <hr>
                        <center><p>Este formulario permite registrar usuarios en el sistema</p> </center>
                        <hr>
                        <div class="row">
                            <div class="col-6">
                                <form action="" method="POST">
                                    <div>
                                        <label for="">Nombre</label>
                                        <input name="Nombre" class="form-control" type="text" placeholder="Ingrese su nombre" required>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">RUT</label>
                                        <input maxlength="14" name="RUT" class="form-control" type="text" name="" id="" placeholder="Ingrese su RUT" required>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">Dirección</label>
                                        <input name="Direccion" class="form-control" type="text" placeholder="Ingrese su domicilio" required>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">Correo electrónico</label>
                                        <input name="Correo" class="form-control" type="email" placeholder="Ingrese su correo electrónico" required>
                                    </div>
                            </div>
                            <div class="col-6">
                                    <div>
                                        <label for="">Apellidos</label>
                                        <input name="Apellido" class="form-control" placeholder="Ingrese apellidos" type="text" id="" required>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">Comuna</label>
                                        <select class="form-control" name="comuna" id="" required>
                                            <option value="">Ingrese su comuna</option>
                                            <option value="">Chiguayante</option>
                                            <option value="">Coronel</option>
                                            <option value="">Florida</option>
                                            <option value="">Hualpén</option>
                                            <option value="">Hualqui</option>
                                            <option value="">Lota</option>
                                            <option value="">Penco</option>
                                            <option value="">San Pedro de la Paz</option>
                                            <option value="">Santa Juana</option>
                                            <option value="">Talcahuano</option>
                                            <option value="">Lota</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">Contraseña</label>
                                        <input name="Contraseña" class="form-control" type="text" placeholder="Ingrese su contraseña" required>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="">Teléfono</label>
                                        <input name="Telefono" class="form-control" type="number" placeholder="Ingrese su teléfono" id="" required>
                                    </div>
                            </div>
                        </div>
                            <br>
                            <button name="botoncito" id="botoncito" type="submit" class="form-control">Registrar</button>
                        </form>
                    </div>
                </div>

            </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
